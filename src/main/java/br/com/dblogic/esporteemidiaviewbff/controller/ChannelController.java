package br.com.dblogic.esporteemidiaviewbff.controller;

import br.com.dblogic.esporteemidiaviewbff.service.SetupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/channel")
public class ChannelController {

    private static final Logger LOG = LoggerFactory.getLogger(ChannelController.class);

    @Autowired
    private SetupService setupService;

    @GetMapping("insertChannel/{setup}/{channel}")
    public ResponseEntity<HttpStatus> insert(@PathVariable("setup") Long setup, @PathVariable("channel") String channel) {
        LOG.info("inserting channel " + channel + "  on setup: " + setup);
        return setupService.insertChannel(setup, channel);
    }

    @GetMapping("removeChannel/{setup}/{channel}")
    public ResponseEntity<HttpStatus> remove(@PathVariable("setup") Long setup, @PathVariable("channel") String channel) {
        LOG.info("removing channel " + channel + "  on setup: " + setup);
        return setupService.removeChannel(setup, channel);
    }

}