package br.com.dblogic.esporteemidiaviewbff.controller;

import br.com.dblogic.esporteemidiaviewbff.dto.SportiveDayDTO;
import br.com.dblogic.esporteemidiaviewbff.service.SportiveDayService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import java.time.format.DateTimeFormatter;

@Controller
@RequestMapping("")
public class MenuController {

    private static final Logger LOG = LoggerFactory.getLogger(MenuController.class);

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

    @Autowired
    private SportiveDayService sportiveDayService;

    @GetMapping(value = {"", "/frontpage"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public String index(Model model) {
        LOG.info("Initial page.");
        model.addAttribute("mindate", sportiveDayService.getMinDate());
        model.addAttribute("maxdate", sportiveDayService.getMaxDate());
        SportiveDayDTO today = sportiveDayService.getToday();
        LOG.info("Today: " + today.getDate());
        model.addAttribute("localDate", today.getDate());
        model.addAttribute("events", today.getSportiveEvents());
        model.addAttribute("url", today.getUrl());
        return "index";
    }

    @GetMapping(value = "/today")
    public String today(Model model) {
        LOG.info("Today's page.");
        SportiveDayDTO today = sportiveDayService.getToday();
        LOG.info("Today: " + today.getDate());
        model.addAttribute("localDate", today.getDate());
        model.addAttribute("events", today.getSportiveEvents());
        model.addAttribute("url", today.getUrl());
        return "listevents";
    }

    @GetMapping(value = "/tomorrow")
    public String tomorrow(Model model) {
        SportiveDayDTO tomorrow = sportiveDayService.getTomorrow();
        LOG.info("Today: " + tomorrow.getDate());
        model.addAttribute("localDate", tomorrow.getDate());
        model.addAttribute("events", tomorrow.getSportiveEvents());
        model.addAttribute("url", tomorrow.getUrl());
        return "listevents";
    }

    @GetMapping(value = "/yesterday")
    public String yesterday(Model model) {
        LOG.info("Yesterday's page if it exists");
        SportiveDayDTO yesterday = sportiveDayService.getYesterday();
        LOG.info("Today: " + yesterday.getDate());
        model.addAttribute("localDate", yesterday.getDate());
        model.addAttribute("events", yesterday.getSportiveEvents());
        model.addAttribute("url", yesterday.getUrl());
        return "listevents";
    }

    @GetMapping(value = "/execution")
    public String execution(Model model) {
        LOG.info("Execution of all sportive day updates");
        sportiveDayService.updateAllSportiveDays();
        model.addAttribute("events", sportiveDayService.getToday().getSportiveEvents());
        model.addAttribute("url", sportiveDayService.getToday().getUrl());
        return "listevents";
    }
}