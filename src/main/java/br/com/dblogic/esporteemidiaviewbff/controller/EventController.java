package br.com.dblogic.esporteemidiaviewbff.controller;

import br.com.dblogic.esporteemidiaviewbff.dto.SportiveDayDTO;
import br.com.dblogic.esporteemidiaviewbff.service.SportiveDayService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@RestController
@RequestMapping(value = "/event")
public class EventController {

    private static final Logger LOG = LoggerFactory.getLogger(EventController.class);

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

    @Autowired
    private SportiveDayService sportiveDayService;

    @GetMapping(value = "/findByDate", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SportiveDayDTO> findByDate(@RequestParam String localDate) {
        LOG.info("get events by date");
        SportiveDayDTO dto = sportiveDayService.getByDate(LocalDate.parse(localDate, formatter));
        return new ResponseEntity(dto, HttpStatus.OK);
    }

}
