package br.com.dblogic.esporteemidiaviewbff.controller;

import br.com.dblogic.esporteemidiaviewbff.service.ChannelService;
import br.com.dblogic.esporteemidiaviewbff.service.SetupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/setup")
public class SetupController {

    private static final Logger LOG = LoggerFactory.getLogger(SetupController.class);

    @Autowired
    private ChannelService channelService;

    @Autowired
    private SetupService setupService;

    @GetMapping(value = "/channel", produces = MediaType.APPLICATION_JSON_VALUE)
    public String setup(Model model) {
        LOG.info("going setup");
        model.addAttribute("channels", channelService.getChannelsFromSetup(1L));
        model.addAttribute("setup", 1L);
        return "setup";
    }
}