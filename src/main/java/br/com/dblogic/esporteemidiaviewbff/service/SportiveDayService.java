package br.com.dblogic.esporteemidiaviewbff.service;

import br.com.dblogic.esporteemidiaviewbff.client.SportiveDayClient;
import br.com.dblogic.esporteemidiaviewbff.dto.SportiveDayDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class SportiveDayService {

    private static final Logger LOG = LoggerFactory.getLogger(SportiveDayService.class);

    @Autowired
    public SportiveDayClient sportiveDayClient;

    public SportiveDayDTO getToday() {
        LOG.info("get today");
        SportiveDayDTO sportiveDay = sportiveDayClient.getToday();
        return sportiveDay;
    }

    public SportiveDayDTO getTomorrow() {
        LOG.info("get tomorrow");
        SportiveDayDTO sportiveDay = sportiveDayClient.getTomorrow();
        return sportiveDay;
    }

    public SportiveDayDTO getYesterday() {
        LOG.info("get yesterday");
        SportiveDayDTO sportiveDay = sportiveDayClient.getYesterday();
        return sportiveDay;
    }

    public SportiveDayDTO getByDate(LocalDate localDate) {
        LOG.info("get by date");
        SportiveDayDTO sportiveDay = sportiveDayClient.getByDate(localDate);
        return sportiveDay;
    }

    public SportiveDayDTO updateAllSportiveDays() {
        LOG.info("update all sportive days");
        SportiveDayDTO sportiveDay = sportiveDayClient.updateAllSportiveDays();
        return sportiveDay;
    }

    public String getMinDate() {
        LOG.info("gets the minimun date");
        return sportiveDayClient.getMinDate();
    }

    public String getMaxDate() {
        LOG.info("gets the maximum date");
        return sportiveDayClient.getMaxDate();
    }
}