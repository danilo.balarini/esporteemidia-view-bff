package br.com.dblogic.esporteemidiaviewbff.service;

import br.com.dblogic.esporteemidiaviewbff.client.SetupClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class SetupService {

    @Autowired
    private SetupClient setupClient;

    public ResponseEntity<HttpStatus> insertChannel(Long setup, String channel) {
        return setupClient.insertChannel(setup, channel);
    }

    public ResponseEntity<HttpStatus> removeChannel(Long setup, String channel) {
        return setupClient.removeChannel(setup, channel);
    }

}