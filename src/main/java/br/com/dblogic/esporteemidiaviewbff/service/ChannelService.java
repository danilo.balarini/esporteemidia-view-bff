package br.com.dblogic.esporteemidiaviewbff.service;

import br.com.dblogic.esporteemidiaviewbff.client.ChannelClient;
import br.com.dblogic.esporteemidiaviewbff.dto.ChannelDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ChannelService {

    private static final Logger LOG = LoggerFactory.getLogger(ChannelService.class);

    @Autowired
    private ChannelClient channelClient;

    public List<ChannelDTO> getChannelsFromSetup(Long id) {
        LOG.info("getChannelsFromSetup setup id: " + id);
        Set<String> channelsSet = channelClient.findChannelsBySetupId(id);
        return findDistinctChannels().stream()
                    .map(c -> new ChannelDTO(0L, c, channelsSet.contains(c)))
                    .sorted(Comparator.comparing(ChannelDTO::getName, String::compareToIgnoreCase))
                    .collect(Collectors.toList());
    }

    private Set<String> findDistinctChannels() {
        LOG.info("finding distinct channels with findDistinctChannels()");
        return channelClient.findDistinctChannels();
    }

}