package br.com.dblogic.esporteemidiaviewbff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class EsporteemidiaViewBffApplication {

	public static void main(String[] args) {
		SpringApplication.run(EsporteemidiaViewBffApplication.class, args);
	}

}