package br.com.dblogic.esporteemidiaviewbff.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class SportiveEventDTO extends DateAuditDTO {

    private Long id;

    private String name;

    private LocalDateTime dateTime;

    private String description;

    private List<ChannelDTO> channels = new ArrayList<>();

    private SportiveDayDTO sportiveDay;

    public SportiveEventDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ChannelDTO> getChannels() {
        return channels;
    }

    public void setChannels(List<ChannelDTO> channels) {
        this.channels = channels;
    }

    public void addChannel(ChannelDTO channel) {
        channels.add(channel);
        channel.setSportiveEvent(this);
    }

    public void removeChannel(ChannelDTO channel) {
        channels.remove(channel);
        channel.setSportiveEvent(null);
    }

    public SportiveDayDTO getSportiveDay() {
        return sportiveDay;
    }

    public void setSportiveDay(SportiveDayDTO sportiveDay) {
        this.sportiveDay = sportiveDay;
    }

}