package br.com.dblogic.esporteemidiaviewbff.dto;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SportiveDayDTO extends DateAuditDTO {

    private Long id;

    private String url;

    private LocalDate date;

    private String text;

    private List<SportiveEventDTO> sportiveEvents = new ArrayList<>();

    private boolean error;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<SportiveEventDTO> getSportiveEvents() {
        return sportiveEvents;
    }

    public void setSportiveEvents(List<SportiveEventDTO> sportiveEvents) {
        this.sportiveEvents = sportiveEvents;
    }

    public void addSportiveEvent(SportiveEventDTO sportiveEvent) {
        sportiveEvents.add(sportiveEvent);
        sportiveEvent.setSportiveDay(this);
    }

    public void removeSportiveEvent(SportiveEventDTO sportiveEvent) {
        sportiveEvents.remove(sportiveEvent);
        sportiveEvent.setSportiveDay(null);
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getDateLocale() {
        return (date == null) ? "" : date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }

    public String getDayName() {
        return (date == null) ? "" : date.format(DateTimeFormatter.ofPattern("EEEE", new Locale("pt", "BR")));
    }

}