package br.com.dblogic.esporteemidiaviewbff.dto;

public class ChannelDTO extends DateAuditDTO {

    private Long id = 0L;

    private String name = "";

    private boolean selected = false;

    private SportiveEventDTO sportiveEvent = new SportiveEventDTO();

    public ChannelDTO() {

    }

    public ChannelDTO(Long id, String name, boolean selected) {
        this.id = id;
        this.name = name;
        this.selected = selected;
    }

    public ChannelDTO(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public SportiveEventDTO getSportiveEvent() {
        return sportiveEvent;
    }

    public void setSportiveEvent(SportiveEventDTO sportiveEvent) {
        this.sportiveEvent = sportiveEvent;
    }

}