package br.com.dblogic.esporteemidiaviewbff.dto;

import java.time.Instant;

abstract class DateAuditDTO {

    private Instant createdAt = Instant.now();

    private Instant updatedAt = Instant.now();

}