package br.com.dblogic.esporteemidiaviewbff.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Set;

@FeignClient(value = "channel", url = "${url.channel}")
public interface ChannelClient {

    @GetMapping(value = "/distinct")
    Set<String> findDistinctChannels();

    @GetMapping(value = "/setup/{id}")
    Set<String> findChannelsBySetupId(@PathVariable("id") Long id);

}