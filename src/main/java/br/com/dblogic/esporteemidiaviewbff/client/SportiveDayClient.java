package br.com.dblogic.esporteemidiaviewbff.client;

import br.com.dblogic.esporteemidiaviewbff.dto.SportiveDayDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.time.LocalDate;

@FeignClient(value = "sportiveday", url = "${url.sportiveday}")
public interface SportiveDayClient {

    @GetMapping(value = "/today")
    SportiveDayDTO getToday();

    @GetMapping(value = "/tomorrow")
    SportiveDayDTO getTomorrow();

    @GetMapping(value = "/yesterday")
    SportiveDayDTO getYesterday();

    @PostMapping(value = "/getByDate")
    SportiveDayDTO getByDate(LocalDate localDate);

    @GetMapping(value = "/updateAllSportiveDays")
    SportiveDayDTO updateAllSportiveDays();

    @GetMapping(value = "/getMinDate")
    String getMinDate();

    @GetMapping(value = "/getMaxDate")
    String getMaxDate();

}