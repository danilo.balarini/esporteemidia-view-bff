package br.com.dblogic.esporteemidiaviewbff.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "setup", url = "${url.setup}")
public interface SetupClient {

    @GetMapping(value = "/insertChannel/{setup}/{channel}")
    ResponseEntity<HttpStatus> insertChannel(@PathVariable("setup") Long setup, @PathVariable("channel") String channel);

    @GetMapping(value = "/removeChannel/{setup}/{channel}")
    ResponseEntity<HttpStatus> removeChannel(@PathVariable("setup") Long setup, @PathVariable("channel") String channel);

}